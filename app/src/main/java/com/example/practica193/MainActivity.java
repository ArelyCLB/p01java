package com.example.practica193;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnSaludar;
    private Button btnLimpiar, btnCerrar;
    private TextView lblSaludo;
    private EditText txtSaludo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnSaludar = (Button) findViewById(R.id.btnSaludar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        lblSaludo = (TextView) findViewById(R.id.lblSaludo);
        txtSaludo = (EditText) findViewById(R.id.txtSaludo);

        btnSaludar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtSaludo.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this,"Favor de ingresar el nombre",
                    Toast.LENGTH_SHORT).show();

                }
                else {
                    String saludar = txtSaludo.getText().toString();
                    lblSaludo.setText("Hola "+saludar+" Como estas?");
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblSaludo.setText("");
                txtSaludo.setText("");
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

}